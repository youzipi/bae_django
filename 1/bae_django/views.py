#!/usr/bin/env python
# coding: utf-8
"""
Created on 7/9/13
"""
from django.http import HttpResponse

__author__ = 'honghe'

def home(request):
    import django
    return HttpResponse("<h1>Honghe home page " + django.get_version() + \
        "!</h1> <a href=\"hello\">hello</a>.")